#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Nuntius v1.0
# Copyright (C) 2019 Felix v. Oertzen
# felix@von-oertzen-berlin.de

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#
# Language checked my Nitschtr.
#

import os
import threading
import time
import logging
import io
import MailUtilites
import json
import sys
import traceback

from telegram.ext import *
from telegram import *
from pathlib import Path

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logging.info("Starting...")
updater = Updater(token="913645868:AAF2DVcASt_vk4Eaoy0Ik-eLS7ZXMErDi8g", use_context=True)
dispatcher = updater.dispatcher

m = None
mainPath = ""


def __not_designed_for(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text="Hi, I'm Nuntius,\n"
                                  "and I'm not designed for direct chat use.\n"
                                  "Please add me to a telegram group; There you're able to configure me.\n")


def message_handler(update, context):
    global m
    if len(update.message.new_chat_members) > 0:
        if update.message.new_chat_members[0].username == "Nuntius_mail_bot":
            context.bot.send_message(chat_id=update.effective_chat.id, text="""Hi, I'm Nuntius!
I'm going to help you communicate with people in groups who don't use Telegram.
Just add some e-mail addresses with the following command: /add [address]

For sending text messages please put /send in front of your message; Images I will send every time.
            """)

    if update.message.text is None:
        return
    if update.message.chat.type == "group" or update.message.chat.type == "supergroup":
        try:
            if not (update.message.text.startswith("/")):
                return

            if update.message.text.startswith("/add"):
                mails = update.message.text[update.message.text.index(" "):].strip()
                mails = [ma.strip() for ma in mails.split(",")]
                for mail in mails:
                    add_mail_address_to_group(update.effective_chat.id, mail)
                context.bot.send_message(chat_id=update.effective_chat.id,
                                         text="Okay, added.")

            if len(get_mail_addresses_for_group(update.effective_chat.id)) < 1:
                context.bot.send_message(chat_id=update.effective_chat.id,
                                         text="Ooops.\nYou need to tell me whom to send messages.\n"
                                              "Without e-mail addresses I'm useless.\nSee /add ...")
                return

            if update.message.text.startswith("/send"):
                cont = update.message.text

                if "/send@Nuntius_mail_bot" in cont:
                    cont = cont.replace("/send@Nuntius_mail_bot", "")
                elif "/send" in cont:
                    cont = cont.replace("/send", "")

                if cont.strip() is "":
                    context.bot.send_message(chat_id=update.effective_chat.id,
                                             text="Tsss, I will not send empty messages.")
                    return

                message = context.bot.send_message(chat_id=update.effective_chat.id, text="Searching for consignees...")
                time.sleep(1)
                context.bot.edit_message_text(chat_id=update.effective_chat.id, message_id=message.message_id,
                                              text="Preparing and sending emails...")

                m.send_mail(
                    group_name=update.effective_chat.title,
                    chat_id=update.effective_chat.id,
                    sender=update.message.from_user.first_name + " " + update.message.from_user.last_name + (" (" +
                           update.message.from_user.username + ")") if update.message.from_user.username is not None else "",
                    consignees=get_mail_addresses_for_group(update.effective_chat.id),
                    content=[cont])
                context.bot.edit_message_text(chat_id=update.effective_chat.id, message_id=message.message_id,
                                              text="Done...")

            if update.message.text.startswith("/remove"):
                mails = update.message.text[update.message.text.index(" "):].strip()
                mails = [ma.strip() for ma in mails.split(",")]
                for mail in mails:
                    # print(mail)
                    i = del_mail_address_from_group(update.effective_chat.id, mail)
                    if i == 0:
                        context.bot.send_message(chat_id=update.effective_chat.id, text="`" + mail + "` is removed.",
                                                 parse_mode=ParseMode.MARKDOWN)
                    else:
                        context.bot.send_message(chat_id=update.effective_chat.id,
                                                 text="`" + mail + "` is not on the list.",
                                                 parse_mode=ParseMode.MARKDOWN)

            if update.message.text.startswith("/list"):
                message = context.bot.send_message(chat_id=update.effective_chat.id, text="I'm going to take a look...")
                a = get_mail_addresses_for_group(update.effective_chat.id)
                context.bot.edit_message_text(chat_id=update.effective_chat.id, message_id=message.message_id,
                                              text="These people are on my list:\n" + "".join(
                                                  ["`" + b + "`\n" for b in a])
                                              , parse_mode=ParseMode.MARKDOWN)
        except Exception as e:
            em = context.bot.send_message(chat_id=update.effective_chat.id,
                                          text="Error: " + str(e))
            traceback.print_exc()
            time.sleep(20)
            context.bot.edit_message_text(chat_id=update.effective_chat.id, message_id=em.message_id,
                                          text="...something went wrong...")

    elif update.message.chat.type == "private":
        __not_designed_for(update, context)


def image(update, context):
    b = io.BytesIO()
    message = context.bot.send_message(chat_id=update.effective_chat.id, text="Downloading...")
    (context.bot.get_file(update['message']['photo'][-1]['file_id'])).download(out=b)
    context.bot.edit_message_text(chat_id=update.effective_chat.id, message_id=message.message_id,
                                  text="Sending...")
    m.send_mail("Groupy",
                update.effective_chat.id,
                update.message.from_user.first_name + " " +
                update.message.from_user.last_name + (" (@" +
                update.message.from_user.username + ")") if update.message.from_user.username is not None else ""
                , get_mail_addresses_for_group(update.effective_chat.id),
                [update.message.caption, b.getvalue()])
    context.bot.edit_message_text(chat_id=update.effective_chat.id, message_id=message.message_id,
                                  text="Done...")


def get_mail_addresses_for_group(group_id):
    with open(mainPath + 'data.json', 'rb') as f:
        try:
            data = json.load(f)
        except:
            return []
        if not (str(group_id) in data.keys()):
            return []
        return data[str(group_id)]


def add_mail_address_to_group(group_id, email):
    with open(mainPath + 'data.json', 'r+') as f:
        try:
            data = json.load(f)
        except:
            data = {}
        if str(group_id) in data:
            data[str(group_id)].append(email)
        else:
            data[str(group_id)] = [email]

        f.seek(0)
        json.dump(data, f, indent=4)
        f.truncate()


def del_mail_address_from_group(group_id, mail):
    with open(mainPath + 'data.json', 'r+') as f:
        data = json.load(f)
        if str(group_id) in data:
            try:
                data[str(group_id)].remove(mail)
            except ValueError:
                return -2
        else:
            return -1
        f.seek(0)
        json.dump(data, f, indent=4)
        f.truncate()
        return 0


def get_mails():
    global m
    while True:
        mails = m.get_mails()
        logging.info("Fetching mails...")
        logging.info("...found " + str(len(mails)) + " new mails...")
        for mail in mails:
            sub = mail['subject']
            id = sub[sub.index("id:") + 3:-1].strip()
            mailFrom = frm = mail['from']
            msg = MailUtilites.parse_mail(mail).splitlines()
            ms = []

            for line in msg:
                if not (line.startswith(">") or line == ""):
                    ms.append(line)

            if "<" in mailFrom and ">" in mailFrom:
                frm = mailFrom[mailFrom.index("<") + 1:mailFrom.index(">")]
            if frm in get_mail_addresses_for_group(id):
                dispatcher.bot.send_message(chat_id=id,
                                            text=mailFrom + ":\n" + "\n".join(ms))
        logging.info("Finished fetching mails.")
        time.sleep(1800)


if len(sys.argv) > 1:
    mainPath = sys.argv[1]
    if mainPath[-1] != os.pathsep:
        mainPath = mainPath + os.sep
Path(mainPath + 'data.json').touch()

try:
    with open(mainPath + "credentials.json", "rb") as datafile:
        settings = json.load(datafile)
        m = MailUtilites.MailService \
            (settings['smtp_server'], settings['pop3_server'],
             settings['user_name'],
             settings['password'])
except KeyError:
    print("Problem while opening settings file; please check 'credentials.json'")
    exit(0)

images = MessageHandler(Filters.photo, image)
dispatcher.add_handler(images)

handler = MessageHandler(Filters.all, message_handler)
dispatcher.add_handler(handler)

mails = (threading.Thread(target=get_mails, daemon=True))
mails.start()

try:
    logging.info("...start polling...")
    updater.start_polling()
except KeyboardInterrupt:
    pass
